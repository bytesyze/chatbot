import discord
import logging
import random
import json
from mysql_connection import SQL_Connection

class ChatBot(discord.Client):
    CHANNEL_ID = "354335248210722816"
    use_tts = False;

    async def say(self, message):
        print("Sending {0}".format(message))
        self.previous_message = message
        await self.send_message(self.get_channel(self.CHANNEL_ID), message, tts=self.use_tts)

    async def on_ready(self):
        print("Logged in as {0}".format(self.user))
        self.sql = SQL_Connection()
        await self.say("I am ready!")

    async def on_message(self, message):
        if (message.author != self.user):
            if "x3@>X!/2X`43=!n'W7mY2rr" in message.content:
                self.use_tts = not self.use_tts
                await self.say("Set TTS mode to " + str(self.use_tts))
                return
            print("Message from {0.author.name}: {0.content}".format(message))
            
            # SENDING INFORMATION TO THE SERVER (LOGGING THE RESPONSE)
            if self.sql.query("SELECT * FROM responses WHERE text = '{0}' AND response_to = '{1}'".format(message.content, self.previous_message)) == ():
                # print("Not a previously logged response")
                self.sql.query("INSERT INTO responses VALUES ('{0}', '{1}', 1)".format(message.content, self.previous_message))
            else:
                new_frequency = self.sql.query("SELECT frequency FROM responses WHERE text = '{0}' AND response_to = '{1}'".format(message.content, self.previous_message))[0][0] + 1
                self.sql.query("UPDATE responses SET frequency = {0} WHERE text = '{1}' AND response_to = '{2}'".format(new_frequency, message.content, self.previous_message))
            await client.change_presence(game = discord.Game(name = "with " + message.author.name))
            # --------------------------------------------------------------- #

            # DECIDING ON THE BOT'S RESPONSE (RESPONDING TO THE USER)
            print(self.sql.query("SELECT text FROM responses WHERE response_to = '{0}' ORDER BY frequency DESC LIMIT 5".format(message.content)))
            common_responses = self.sql.query("SELECT text FROM responses WHERE response_to = '{0}' ORDER BY frequency DESC LIMIT 5".format(message.content))[0]
            await self.say(random.choice(common_responses))
            # --------------------------------------------------------------- #

with open('credentials.json') as data_file:
    credentials = json.load(data_file)
client = ChatBot()
logging.basicConfig(level=logging.INFO)
client.run(credentials['bot_token'])