import pymysql as sql
import json

with open('credentials.json') as data_file:
    credentials = json.load(data_file)

class SQL_Connection: 
    def __init__(self, user = credentials['sql_user'], password = credentials['sql_passwd']):
        self.user = user
        self.password = password
        self.db = sql.connect(host = "160.153.16.9", port = 3306, user = self.user, passwd = self.password, db = "chatbot_data", autocommit = True)
        self.cursor = self.db.cursor()

    def query(self, query):
        self.db.ping()
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def close():
        self.db.close()